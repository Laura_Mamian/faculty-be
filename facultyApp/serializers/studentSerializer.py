from rest_framework import serializers
from facultyApp.models.studentModel import Student
#from facultyApp.models.registrationModel import Registration
#from facultyApp.serializers.registrationSerializer import RegistrationSerializer


class StudentSerializer(serializers.ModelSerializer):

    #registrations = RegistrationSerializer(many=True)

    class Meta:
        model = Student
        fields = "__all__"
