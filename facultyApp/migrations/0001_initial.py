# Generated by Django 3.2.8 on 2021-10-29 19:14

from django.db import migrations, models
import facultyApp.models.studentModel


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='Registration',
            fields=[
                ('regId', models.AutoField(primary_key=True, serialize=False)),
                ('student', models.CharField(max_length=10, verbose_name=facultyApp.models.studentModel.Student)),
                ('regCourse', models.CharField(max_length=30, verbose_name='Course')),
                ('regCourseDate', models.CharField(max_length=30, verbose_name='CourseDate')),
                ('regDate', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('username', models.CharField(max_length=15, unique=True, verbose_name='username')),
                ('password', models.CharField(max_length=256, verbose_name='password')),
                ('stuEmail', models.EmailField(max_length=50, verbose_name='Email')),
                ('stuName', models.CharField(max_length=30, verbose_name='Name')),
                ('stuLastName', models.CharField(max_length=30, verbose_name='Last Name')),
                ('stuPhone', models.CharField(max_length=15, null=True, verbose_name='Phone')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
