from django.contrib import admin
from .models.registrationModel import Registration
from .models.studentModel import Student

admin.site.register(Student)
admin.site.register(Registration)