from rest_framework import generics, serializers,status
from rest_framework.response import Response
from facultyApp.models.registrationModel import Registration
from facultyApp.serializers.registrationSerializer import RegistrationSerializer


class RegistrationDetailView(generics.ListAPIView):
    serializer_class = RegistrationSerializer
    def get_queryset(self):
      queryset = Registration.objects.all()
      return queryset
