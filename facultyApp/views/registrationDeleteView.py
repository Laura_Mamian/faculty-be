from django.db.models import query
from rest_framework import generics, serializers
from rest_framework.response import Response
from rest_framework import status

from facultyApp.models.registrationModel import Registration
from facultyApp.serializers.registrationSerializer import RegistrationSerializer


class RegistrationDeleteView(generics.DestroyAPIView):
    serializers_class = RegistrationSerializer
    queryset = Registration.objects.all()
