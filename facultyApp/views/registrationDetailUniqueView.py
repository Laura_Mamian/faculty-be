from django.db.models import query
from rest_framework import generics
from rest_framework.response import Response
from facultyApp.models.registrationModel import Registration
from facultyApp.serializers.registrationSerializer import RegistrationSerializer


class RegistrationDetailUniqueView(generics.ListAPIView):
    serializer_class = RegistrationSerializer
    def get_queryset(self):
        student = self.kwargs["pk"]
        queryset = Registration.objects.filter(student= student )
        return queryset


