from functools import partial
from django.db.models import query
from rest_framework import generics
from rest_framework.response import Response
from rest_framework import status

from facultyApp.serializers.registrationSerializer import RegistrationSerializer
from facultyApp.models.registrationModel import Registration


class RegistrationUpdateView(generics.UpdateAPIView):
    queryset = Registration.objects.all()
    serializer_class = RegistrationSerializer
    lookup_field = 'pk'

    def patch(self, request, pk, format=None):
        registration = self.get_object()
        serializer = RegistrationSerializer(
            registration, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
