from django.db import models
from facultyApp.models.studentModel import Student


class Registration(models.Model):
    regId = models.AutoField(primary_key=True)
    student = models.CharField(
        Student, max_length=10, null=False)
    # related_name="registrations", on_delete=models.CASCADE)
    regCourse = models.CharField("Course", max_length=30, null=False)
    regCourseDate = models.CharField("CourseDate", max_length=30)
    regDate = models.DateTimeField(auto_now=True)
