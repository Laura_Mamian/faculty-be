from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password


class StudentManager (BaseUserManager):
    def create_student(self, username, password=None):
        """
        Creates and saves a student with the given username and password
        """
        if not username:
            raise ValueError('Usuarios deben tener un nombre de usuario')
        user = self.model(username=username)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password):
        """
        Creates and saves a superuser with the given username and password

        """
        user = self.create_student(username=username, password=password)
        user.is_admin = True
        user.is_save(using=self._db)
        return user


class Student(AbstractBaseUser, PermissionsMixin):
    id = models.BigAutoField(primary_key=True)
    username = models.CharField('username', max_length=15, null=False, unique=True)
    password = models.CharField('password', max_length=256, null=False)
    stuEmail = models.EmailField('Email', max_length=50)
    stuName = models.CharField('Name', max_length=30, null=False)
    stuLastName = models.CharField('Last Name', max_length=30, null=False)
    stuPhone = models.CharField('Phone', max_length=15, null=True)

    def save(self, **kwargs):
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN'
        self.password = make_password(self.password, some_salt)
        super().save(**kwargs)

    objects = StudentManager()
    USERNAME_FIELD = 'username'
