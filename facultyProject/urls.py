"""facultyProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from facultyApp import views as facultyViews
from rest_framework_simplejwt.views import (
    TokenObtainPairView, TokenRefreshView)

urlpatterns = [
    #path('admin/', admin.site.urls),
    # use credentials to return tokens
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),  # generate new access token
    path('student/', facultyViews.StudentCreateView.as_view()),
    path('student/<int:pk>/', facultyViews.StudentDetailView.as_view()),
    path('addRegistration/', facultyViews.RegistrationCreateView.as_view()),
    path('updateRegistration/<int:pk>',
         facultyViews.RegistrationUpdateView.as_view()),
    path('deleteRegistration/<int:pk>',
         facultyViews.RegistrationDeleteView.as_view()),
    path('registrationsDetails/', facultyViews.RegistrationDetailView.as_view()),
    path('registrationsDetails/<int:pk>/',
         facultyViews.RegistrationDetailUniqueView.as_view()),
]
